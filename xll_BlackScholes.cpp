#include "Volar/Analytics/BlackScholes.h"
#include "xllvola.h"

using namespace xll;

using namespace volar::v1;

AddIn xai_BlackScholes_value(
	Function(XLL_DOUBLE, "xll_BlackScholes_value", CATEGORY ".BlackScholes.value")
	.Arguments({
		Arg(XLL_DOUBLE, "S", "is the spot."),
		Arg(XLL_DOUBLE, "K", "is the strike."),
		Arg(XLL_DOUBLE, "T", "is the time in years to expiration."),
		Arg(XLL_DOUBLE, "vol", "is the volatility."),
		Arg(XLL_DOUBLE, "r", "is the rate."),
		Arg(XLL_DOUBLE, "q", "is the dividend yield."),
		Arg(XLL_BOOL, "isCall", "call/put flag"),
		})
	.FunctionHelp("The theoretical Black-Scholes option value.")
	.Category(CATEGORY)
);
double WINAPI xll_BlackScholes_value(double S, double K, double T, double vol, double r, double q, BOOL isCall)
{
#pragma XLLEXPORT

	return BlackScholes::value(S, K, T, vol, r, q, isCall);
}